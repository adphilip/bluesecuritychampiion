# BlueSecurityChampion

Classes:
https://academy.ing.net/learn/security-blue

Git exam app:
https://gitlab.ing.net/ITS-Security-BE/security-champions-workshop

Burp:
https://portswigger.net/burp/releases/professional-community-2020-2
Search in browser proxy for browser to be exposed to Burp. Setup usually 127.0.0.1:<port>

Examples from Kali:
